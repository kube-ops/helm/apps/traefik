# TCP route example

```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRouteTCP
metadata:
  name: ssh
spec:
  entryPoints:
    - ssh
  routes:
    - match: HostSNI(`*`)
      services:
        - name: my-ssh-service-name
          port: 22
          namespace: default
          weight: 10
          terminationDelay: 400
          proxyProtocol:
            version: 1
  tls:
    secretName: supersecret
    options:
      name: opt
      namespace: default
    certResolver: foo
    domains:
      - main: example.net
        sans:
          - a.example.net
          - b.example.net
    passthrough: false
```
