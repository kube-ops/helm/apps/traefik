# UDP route example

```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRouteUDP
metadata:
  name: coredns
spec:
  entryPoints:
    - dns
  routes:
    - services:
        - name: coredns
          port: 53
          namespace: kube-system
```
