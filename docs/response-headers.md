# Custom Response Headers

```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  name: custom-response-headers
  namespace: ingress
spec:
  headers:
    customResponseHeaders:
      Forwarded: ""
      Server: nginx
      Via: ""
      X-Amz-Id-2: ""
      X-Amz-Request-Id: ""
      X-Amz-Meta-Server-Side-Encryption: ""
      X-Amz-Server-Side-Encryption: ""
      Permissions-Policy: >
        accelerometer=(),
        battery=(),
        camera=(),
        geolocation=(),
        gyroscope=(),
        magnetometer=(),
        microphone=(),
        usb=(),
        vibrate=(),
        vr=()
```
