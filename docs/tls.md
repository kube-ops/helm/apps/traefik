# TLS

## TLSOption Example

```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: TLSOption
metadata:
  name: intermediate
spec:
  minVersion: VersionTLS12
  # Generated with https://ssl-config.mozilla.org/#server=traefik&version=2.3.0&config=intermediate&guideline=5.6
  cipherSuites:
    - TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256
    - TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
    - TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305
    - TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305
    - TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384
    - TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
  sniStrict: true
  preferServerCipherSuites: true
```

## TLSStore Example

```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: TLSStore
metadata:
  name: mytlsstore
spec:
  defaultCertificate:
    secretName: tls-cert
```
