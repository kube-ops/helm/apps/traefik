# Basic Auth

## Create Secret

- Use `htpasswd` from Apache utils for generating auth secret:

```console
$ htpasswd -nb user password
user:$apr1$s53fUTTX$8ZwcaWDINlUlZ79N6IVb30
```

- Create secret:

```console
$ kubectl create secret generic basic-auth-test --from-literal=users=user:$apr1$s53fUTTX$8ZwcaWDINlUlZ79N6IVb30
secret/basic-auth-test created
```

- Verify created secret:

```console
$ kubectl describe secret basic-auth-test
Name:         basic-auth-test
Namespace:    default
Labels:       <none>
Annotations:  <none>

Type:  Opaque

Data
====
users:  26 bytes
```

## Create Middleware

```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  name: test-auth
spec:
  basicAuth:
    secret: basic-auth-test
```

- Customize realm and auth header if needed:

```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  name: test-auth
spec:
  basicAuth:
    realm: MyRealm
    headerField: X-WebAuth-User
```

## Using Basic Auth Middleware

```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: private-route
spec:
  routes:
    - kind: Rule
      match: Host(`staff.example.com`)
      middlewares:
        - name: test-auth
      services:
        - name: private-service
```
