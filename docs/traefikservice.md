# TraefikService

## Weighted Round-Robin Example

```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: TraefikService
metadata:
  name: wrr1
spec:
  weighted:
    services:
      - name: wrr2
        kind: TraefikService
        weight: 1
      - name: s3
        weight: 1
        port: 80
---
apiVersion: traefik.containo.us/v1alpha1
kind: TraefikService
metadata:
  name: wrr2
spec:
  weighted:
    services:
      - name: s1
        weight: 1
        port: 80
        # Optional, as it is the default value
        kind: Service
      - name: s3
        weight: 1
        port: 80
```

## Mirroring Example

```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: TraefikService
metadata:
  name: mirror1
spec:
  mirroring:
    name: s1
    port: 80
    mirrors:
      - name: s3
        percent: 20
        port: 80
      - name: mirror2
        kind: TraefikService
        percent: 20
---
apiVersion: traefik.containo.us/v1alpha1
kind: TraefikService
metadata:
  name: mirror2
spec:
  mirroring:
    name: wrr2
    kind: TraefikService
    mirrors:
      - name: s2
        # Optional
        maxBodySize: 2000000000
        # Optional, as it is the default value
        kind: Service
        percent: 20
        port: 80
```
