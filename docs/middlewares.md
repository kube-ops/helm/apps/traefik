# Middlewares

## Permanent Redirection To HTTPS

```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  name: redirect-https
spec:
  redirectScheme:
    scheme: https
    permanent: true
```

## Setup Max Body Size

### Limit Request Body

```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  name: max-body-1gb
spec:
  buffering:
    maxRequestBodyBytes: 1073741824 # 1Gi
    memRequestBodyBytes: 104857600  # 100Mi
```

### Limit Response Body

```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  name: limit
spec:
  buffering:
    memRequestBodyBytes: 5242880   # 5Mi
    maxResponseBodyBytes: 15728640 # 15Mi
```

## Custom Error Page Example

```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  name: custom-errorpage
spec:
  errors:
    status:
      - 500-599
    query: /{status}.html
    service:
      name: whoami
      port: 80
```

## Secure HTTP Headers

```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  name: secure-headers
spec:
  headers:
    browserXssFilter: true
    contentTypeNosniff: true
    frameDeny: false
    customFrameOptionsValue: SAMEORIGIN
    stsSeconds: 31536000
    stsIncludeSubdomains: true
    stsPreload: true
    referrerPolicy: same-origin
    featurePolicy: >
      accelerometer 'none';
      ambient-light-sensor 'none';
      battery 'none';
      camera 'none';
      geolocation 'none';
      gyroscope 'none';
      magnetometer 'none';
      microphone 'none';
      usb 'none';
      vr 'none';
```

## Deny External Hosts

```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  name: deny-external-hosts
spec:
  ipWhiteList:
    sourceRange:
      - 10.0.0.0/8
      - 127.0.0.0/8
      - 172.16.0.0/12
      - 192.168.0.0/16
```

## ForwardAuth

```yaml
# Forward authentication to example.com
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  name: test-auth
spec:
  forwardAuth:
    address: https://example.com/auth
    trustForwardHeader: true
    authResponseHeadersRegex: ^X-
    authResponseHeaders:
      - X-Auth-User
      - X-Secret
    authRequestHeaders:
      - "Accept"
      - "X-CustomHeader"
    tls:
      insecureSkipVerify: true
      caOptional: true
      caSecret: mycasercret
      certSecret: mytlscert
```
