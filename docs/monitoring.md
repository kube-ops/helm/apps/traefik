# Monitoring Traefik

## Metrics service

- Set `metrics.prometheus.enabled` to `true` in `values.yaml`:

```yaml
metrics:
  prometheus:
    enabled: true
```

## ServiceMonitor

- Install [kube-prometheus-stack](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack)
- Set `metrics.prometheus.enabled` to `true` in `values.yaml`
- Set `metrics.serviceMonitor.enabled` to `true` in `values.yaml`

```yaml
metrics:
  prometheus:
    enabled: true
    serviceMonitor:
      enabled: true
```

## Alerts

- Set `metrics.prometheus.rules.enabled` to `true` in `values.yaml`

```yaml
metrics:
  prometheus:
    enabled: true
    serviceMonitor:
      enabled: true
    rules:
      enabled: true
```

## Dashboard for Grafana

- Enable grafana sidecar
- Set `metrics.prometheus.grafanaDashboard.enabled` to `true` in `values.yaml`

```yaml
metrics:
  prometheus:
    enabled: true
    serviceMonitor:
      enabled: true
    grafanaDashboard:
      enabled: true
```

- Or you can manually import [dashboard](https://grafana.com/grafana/dashboards/13165) from Grafana Dashboards
