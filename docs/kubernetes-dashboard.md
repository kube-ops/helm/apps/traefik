# Kubernetes Dashboard Example

Example IngressRoute for [kubernetes-dashboard](https://github.com/kubernetes/dashboard/tree/master/aio/deploy/helm-chart/kubernetes-dashboard)

```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: kubernetes-dashboard
spec:
  entryPoints:
    - https
  routes:
    - match: Host(`kubernetes-dashboard.example.com`)
      kind: Rule
      services:
        - name: kubernetes-dashboard
          port: 443
          scheme: https
      middlewares:
        - name: secure-headers
          namespace: ingress
        - name: deny-external-hosts
          namespace: ingress
  tls:
    certResolver: letsencrypt-route53
    domains:
      - main: kubernetes-dashboard.example.com
    options:
      name: intermediate
      namespace: ingress
```
