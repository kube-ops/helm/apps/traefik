apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  name: traefikservices.traefik.containo.us
spec:
  group: traefik.containo.us
  scope: Namespaced
  names:
    kind: TraefikService
    listKind: TraefikServiceList
    plural: traefikservices
    singular: traefikservice
  versions:
    - name: v1alpha1
      served: true
      storage: true
      schema:
        openAPIV3Schema:
          type: object
          description: >-
            TraefikService is the specification for a service (that an IngressRoute refers to) that is usually not a terminal service (i.e. not a pod of servers),
            as opposed to a Kubernetes Service. That is to say, it usually refers to other (children) services, which themselves can be TraefikServices or Services.
          required:
            - metadata
            - spec
          properties:
            apiVersion:
              type: string
              description: >-
                APIVersion defines the versioned schema of this representation of an object.
                Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values.
                More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources
            kind:
              type: string
              description: >-
                Kind is a string value representing the REST resource this object represents.
                Servers may infer this from the endpoint the client submits requests to.
                Cannot be updated.
                In CamelCase.
                More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds
            metadata:
              type: object
            spec:
              type: object
              description: >-
                ServiceSpec defines whether a TraefikService is a load-balancer of services or a mirroring service.
              properties:
                mirroring:
                  type: object
                  description: >-
                    Mirroring defines a mirroring service, which is composed of a main load-balancer, and a list of mirrors.
                  required:
                    - name
                  properties:
                    kind:
                      type: string
                      enum:
                        - Service
                        - TraefikService
                    maxBodySize:
                      type: integer
                      format: int64
                    mirrors:
                      type: array
                      items:
                        type: object
                        required:
                          - name
                        description: >-
                          MirrorService defines one of the mirrors of a Mirroring service.
                        properties:
                          kind:
                            type: string
                            enum:
                              - Service
                              - TraefikService
                          name:
                            type: string
                            description: >-
                              Name is a reference to a Kubernetes Service object (for a load-balancer of servers),
                              or to a TraefikService object (service load-balancer, mirroring, etc).
                              The differentiation between the two is specified in the Kind field.
                          namespace:
                            type: string
                          passHostHeader:
                            type: boolean
                          percent:
                            type: integer
                          port:
                            anyOf:
                              - type: integer
                              - type: string
                            x-kubernetes-int-or-string: true
                          responseForwarding:
                            type: object
                            description: >-
                              ResponseForwarding holds configuration for the forward of the response.
                            properties:
                              flushInterval:
                                type: string
                          scheme:
                            type: string
                          serversTransport:
                            type: string
                          sticky:
                            type: object
                            description: Sticky holds the sticky configuration.
                            properties:
                              cookie:
                                type: object
                                description: Cookie holds the sticky configuration based on cookie.
                                properties:
                                  httpOnly:
                                    type: boolean
                                  name:
                                    type: string
                                  sameSite:
                                    type: string
                                  secure:
                                    type: boolean
                          strategy:
                            type: string
                          weight:
                            type: integer
                            description: >-
                              Weight should only be specified when Name references a TraefikService object (and to be precise, one that embeds a Weighted Round Robin).
                    name:
                      type: string
                      description: >-
                        Name is a reference to a Kubernetes Service object (for a load-balancer of servers),
                        or to a TraefikService object (service load-balancer, mirroring, etc).
                        The differentiation between the two is specified in the Kind field.
                    namespace:
                      type: string
                    passHostHeader:
                      type: boolean
                    port:
                      anyOf:
                        - type: integer
                        - type: string
                      x-kubernetes-int-or-string: true
                    responseForwarding:
                      type: object
                      description: ResponseForwarding holds configuration for the forward of the response.
                      properties:
                        flushInterval:
                          type: string
                    scheme:
                      type: string
                    serversTransport:
                      type: string
                    sticky:
                      type: object
                      description: Sticky holds the sticky configuration.
                      properties:
                        cookie:
                          type: object
                          description: Cookie holds the sticky configuration based on cookie.
                          properties:
                            httpOnly:
                              type: boolean
                            name:
                              type: string
                            sameSite:
                              type: string
                            secure:
                              type: boolean
                    strategy:
                      type: string
                    weight:
                      type: integer
                      description: >-
                        Weight should only be specified when Name references a TraefikService object (and to be precise, one that embeds a Weighted Round Robin).
                weighted:
                  type: object
                  description: WeightedRoundRobin defines a load-balancer of services.
                  properties:
                    services:
                      type: array
                      items:
                        type: object
                        description: Service defines an upstream to proxy traffic.
                        required:
                          - name
                        properties:
                          kind:
                            type: string
                            enum:
                              - Service
                              - TraefikService
                          name:
                            type: string
                            description: >-
                              Name is a reference to a Kubernetes Service object (for a load-balancer of servers),
                              or to a TraefikService object (service load-balancer, mirroring, etc).
                              The differentiation between the two is specified in the Kind field.
                          namespace:
                            type: string
                          passHostHeader:
                            type: boolean
                          port:
                            anyOf:
                              - type: integer
                              - type: string
                            x-kubernetes-int-or-string: true
                          responseForwarding:
                            type: object
                            description: ResponseForwarding holds configuration for the forward of the response.
                            properties:
                              flushInterval:
                                type: string
                          scheme:
                            type: string
                          serversTransport:
                            type: string
                          sticky:
                            type: object
                            description: Sticky holds the sticky configuration.
                            properties:
                              cookie:
                                type: object
                                description: Cookie holds the sticky configuration based on cookie.
                                properties:
                                  httpOnly:
                                    type: boolean
                                  name:
                                    type: string
                                  sameSite:
                                    type: string
                                  secure:
                                    type: boolean
                          strategy:
                            type: string
                          weight:
                            type: integer
                            description: >-
                              Weight should only be specified when Name references a TraefikService object (and to be precise,
                              one that embeds a Weighted Round Robin).
                    sticky:
                      type: object
                      description: Sticky holds the sticky configuration.
                      properties:
                        cookie:
                          type: object
                          description: Cookie holds the sticky configuration based on cookie.
                          properties:
                            httpOnly:
                              type: boolean
                            name:
                              type: string
                            sameSite:
                              type: string
                            secure:
                              type: boolean
status:
  acceptedNames:
    kind: ""
    plural: ""
  conditions: []
  storedVersions: []
