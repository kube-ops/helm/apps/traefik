apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  name: middlewares.traefik.containo.us
spec:
  group: traefik.containo.us
  scope: Namespaced
  names:
    kind: Middleware
    listKind: MiddlewareList
    plural: middlewares
    singular: middleware
  versions:
    - name: v1alpha1
      served: true
      storage: true
      schema:
        openAPIV3Schema:
          type: object
          description: Middleware is a specification for a Middleware resource.
          required:
            - metadata
            - spec
          properties:
            apiVersion:
              type: string
              description: >-
                APIVersion defines the versioned schema of this representation of an object.
                Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values.
                More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources
            kind:
              type: string
              description: >-
                Kind is a string value representing the REST resource this object represents.
                Servers may infer this from the endpoint the client submits requests to.
                Cannot be updated.
                In CamelCase.
                More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds
            metadata:
              type: object
            spec:
              type: object
              description: MiddlewareSpec holds the Middleware configuration.
              properties:
                addPrefix:
                  type: object
                  description: AddPrefix holds the AddPrefix configuration.
                  properties:
                    prefix:
                      type: string
                basicAuth:
                  type: object
                  description: BasicAuth holds the HTTP basic authentication configuration.
                  properties:
                    headerField:
                      type: string
                    realm:
                      type: string
                    removeHeader:
                      type: boolean
                    secret:
                      type: string
                buffering:
                  type: object
                  description: Buffering holds the request/response buffering configuration.
                  properties:
                    maxRequestBodyBytes:
                      type: integer
                      format: int64
                    maxResponseBodyBytes:
                      type: integer
                      format: int64
                    memRequestBodyBytes:
                      type: integer
                      format: int64
                    memResponseBodyBytes:
                      type: integer
                      format: int64
                    retryExpression:
                      type: string
                chain:
                  type: object
                  description: Chain holds a chain of middlewares.
                  properties:
                    middlewares:
                      type: array
                      items:
                        type: object
                        description: MiddlewareRef is a ref to the Middleware resources.
                        required:
                          - name
                        properties:
                          name:
                            type: string
                          namespace:
                            type: string
                circuitBreaker:
                  type: object
                  description: CircuitBreaker holds the circuit breaker configuration.
                  properties:
                    expression:
                      type: string
                compress:
                  type: object
                  description: Compress holds the compress configuration.
                  properties:
                    excludedContentTypes:
                      type: array
                      items:
                        type: string
                contentType:
                  type: object
                  description: >-
                    ContentType middleware - or rather its unique `autoDetect` option - specifies whether to let the `Content-Type` header,
                    if it has not been set by the backend, be automatically set to a value derived from the contents of the response.
                    As a proxy, the default behavior should be to leave the header alone, regardless of what the backend did with it.
                    However, the historic default was to always auto-detect and set the header if it was nil, and it is going to be kept
                    that way in order to support users currently relying on it.
                    This middleware exists to enable the correct behavior until at least the default one can be changed in a future version.
                  properties:
                    autoDetect:
                      type: boolean
                digestAuth:
                  type: object
                  description: DigestAuth holds the Digest HTTP authentication configuration.
                  properties:
                    headerField:
                      type: string
                    realm:
                      type: string
                    removeHeader:
                      type: boolean
                    secret:
                      type: string
                errors:
                  type: object
                  description: ErrorPage holds the custom error page configuration.
                  properties:
                    query:
                      type: string
                    service:
                      type: object
                      description: Service defines an upstream to proxy traffic.
                      required:
                        - name
                      properties:
                        kind:
                          type: string
                          enum:
                            - Service
                            - TraefikService
                        name:
                          type: string
                          description: >-
                            Name is a reference to a Kubernetes Service object (for a load-balancer of servers), or to a TraefikService object
                            (service load-balancer, mirroring, etc).
                            The differentiation between the two is specified in the Kind field.
                        namespace:
                          type: string
                        passHostHeader:
                          type: boolean
                        port:
                          anyOf:
                            - type: integer
                            - type: string
                          x-kubernetes-int-or-string: true
                        responseForwarding:
                          type: object
                          description: ResponseForwarding holds configuration for the forward of the response.
                          properties:
                            flushInterval:
                              type: string
                        scheme:
                          type: string
                        serversTransport:
                          type: string
                        sticky:
                          type: object
                          description: Sticky holds the sticky configuration.
                          properties:
                            cookie:
                              type: object
                              description: Cookie holds the sticky configuration based on cookie.
                              properties:
                                httpOnly:
                                  type: boolean
                                name:
                                  type: string
                                sameSite:
                                  type: string
                                secure:
                                  type: boolean
                        strategy:
                          type: string
                        weight:
                          type: integer
                          description: >-
                            Weight should only be specified when Name references a TraefikService object (and to be precise, one that embeds a Weighted Round Robin).
                    status:
                      type: array
                      items:
                        type: string
                forwardAuth:
                  type: object
                  description: ForwardAuth holds the http forward authentication configuration.
                  properties:
                    address:
                      type: string
                    authRequestHeaders:
                      type: array
                      items:
                        type: string
                    authResponseHeaders:
                      type: array
                      items:
                        type: string
                    authResponseHeadersRegex:
                      type: string
                    tls:
                      type: object
                      description: ClientTLS holds TLS specific configurations as client.
                      properties:
                        caOptional:
                          type: boolean
                        caSecret:
                          type: string
                        certSecret:
                          type: string
                        insecureSkipVerify:
                          type: boolean
                    trustForwardHeader:
                      type: boolean
                headers:
                  type: object
                  description: Headers holds the custom header configuration.
                  properties:
                    accessControlAllowCredentials:
                      type: boolean
                      description: AccessControlAllowCredentials is only valid if true. False is ignored.
                    accessControlAllowHeaders:
                      type: array
                      description: AccessControlAllowHeaders must be used in response to a preflight request with Access-Control-Request-Headers set.
                      items:
                        type: string
                    accessControlAllowMethods:
                      type: array
                      description: AccessControlAllowMethods must be used in response to a preflight request with Access-Control-Request-Method set.
                      items:
                        type: string
                    accessControlAllowOriginList:
                      type: array
                      description: AccessControlAllowOriginList is a list of allowable origins. Can also be a wildcard origin "*".
                      items:
                        type: string
                    accessControlAllowOriginListRegex:
                      type: array
                      description: >-
                        AccessControlAllowOriginListRegex is a list of allowable origins written following the Regular Expression syntax (https://golang.org/pkg/regexp/).
                      items:
                        type: string
                    accessControlExposeHeaders:
                      type: array
                      description: AccessControlExposeHeaders sets valid headers for the response.
                      items:
                        type: string
                    accessControlMaxAge:
                      type: integer
                      description: AccessControlMaxAge sets the time that a preflight request may be cached.
                      format: int64
                    addVaryHeader:
                      type: boolean
                      description: AddVaryHeader controls if the Vary header is automatically added/updated when the AccessControlAllowOriginList is set.
                    allowedHosts:
                      type: array
                      items:
                        type: string
                    browserXssFilter:
                      type: boolean
                    contentSecurityPolicy:
                      type: string
                    contentTypeNosniff:
                      type: boolean
                    customBrowserXSSValue:
                      type: string
                    customFrameOptionsValue:
                      type: string
                    customRequestHeaders:
                      type: object
                      additionalProperties:
                        type: string
                    customResponseHeaders:
                      type: object
                      additionalProperties:
                        type: string
                    featurePolicy:
                      type: string
                    forceSTSHeader:
                      type: boolean
                    frameDeny:
                      type: boolean
                    hostsProxyHeaders:
                      type: array
                      items:
                        type: string
                    isDevelopment:
                      type: boolean
                    publicKey:
                      type: string
                    referrerPolicy:
                      type: string
                    sslForceHost:
                      type: boolean
                      description: >-
                        Deprecated: use RedirectRegex instead.
                    sslHost:
                      type: string
                      description: >-
                        Deprecated: use RedirectRegex instead.
                    sslProxyHeaders:
                      type: object
                      additionalProperties:
                        type: string
                    sslRedirect:
                      type: boolean
                      description: >-
                        Deprecated: use EntryPoint redirection or RedirectScheme instead.
                    sslTemporaryRedirect:
                      type: boolean
                      description: >-
                        Deprecated: use EntryPoint redirection or RedirectScheme instead.
                    stsIncludeSubdomains:
                      type: boolean
                    stsPreload:
                      type: boolean
                    stsSeconds:
                      type: integer
                      format: int64
                inFlightReq:
                  type: object
                  description: InFlightReq limits the number of requests being processed and served concurrently.
                  properties:
                    amount:
                      type: integer
                      format: int64
                    sourceCriterion:
                      type: object
                      description: >-
                        SourceCriterion defines what criterion is used to group requests as originating from a common source.
                        If none are set, the default is to use the request's remote address field.
                        All fields are mutually exclusive.
                      properties:
                        ipStrategy:
                          type: object
                          description: IPStrategy holds the ip strategy configuration.
                          properties:
                            depth:
                              type: integer
                            excludedIPs:
                              type: array
                              items:
                                type: string
                        requestHeaderName:
                          type: string
                        requestHost:
                          type: boolean
                ipWhiteList:
                  type: object
                  description: IPWhiteList holds the ip white list configuration.
                  properties:
                    ipStrategy:
                      type: object
                      description: IPStrategy holds the ip strategy configuration.
                      properties:
                        depth:
                          type: integer
                        excludedIPs:
                          type: array
                          items:
                            type: string
                    sourceRange:
                      type: array
                      items:
                        type: string
                passTLSClientCert:
                  type: object
                  description: PassTLSClientCert holds the TLS client cert headers configuration.
                  properties:
                    info:
                      type: object
                      description: TLSClientCertificateInfo holds the client TLS certificate info configuration.
                      properties:
                        issuer:
                          type: object
                          description: >-
                            TLSCLientCertificateDNInfo holds the client TLS certificate distinguished name info configuration.
                            See: https://tools.ietf.org/html/rfc3739
                          properties:
                            commonName:
                              type: boolean
                            country:
                              type: boolean
                            domainComponent:
                              type: boolean
                            locality:
                              type: boolean
                            organization:
                              type: boolean
                            province:
                              type: boolean
                            serialNumber:
                              type: boolean
                        notAfter:
                          type: boolean
                        notBefore:
                          type: boolean
                        sans:
                          type: boolean
                        serialNumber:
                          type: boolean
                        subject:
                          type: object
                          description: >-
                            TLSCLientCertificateDNInfo holds the client TLS certificate distinguished name info configuration.
                            See: https://tools.ietf.org/html/rfc3739
                          properties:
                            commonName:
                              type: boolean
                            country:
                              type: boolean
                            domainComponent:
                              type: boolean
                            locality:
                              type: boolean
                            organization:
                              type: boolean
                            province:
                              type: boolean
                            serialNumber:
                              type: boolean
                    pem:
                      type: boolean
                plugin:
                  type: object
                  additionalProperties:
                    x-kubernetes-preserve-unknown-fields: true
                rateLimit:
                  type: object
                  description: RateLimit holds the rate limiting configuration for a given router.
                  properties:
                    average:
                      type: integer
                      format: int64
                    burst:
                      type: integer
                      format: int64
                    period:
                      anyOf:
                        - type: integer
                        - type: string
                      x-kubernetes-int-or-string: true
                    sourceCriterion:
                      type: object
                      description: >-
                        SourceCriterion defines what criterion is used to group requests as originating from a common source.
                        If none are set, the default is to use the request's remote address field.
                        All fields are mutually exclusive.
                      properties:
                        ipStrategy:
                          type: object
                          description: IPStrategy holds the ip strategy configuration.
                          properties:
                            depth:
                              type: integer
                            excludedIPs:
                              type: array
                              items:
                                type: string
                        requestHeaderName:
                          type: string
                        requestHost:
                          type: boolean
                redirectRegex:
                  type: object
                  description: RedirectRegex holds the redirection configuration.
                  properties:
                    permanent:
                      type: boolean
                    regex:
                      type: string
                    replacement:
                      type: string
                redirectScheme:
                  type: object
                  description: RedirectScheme holds the scheme redirection configuration.
                  properties:
                    permanent:
                      type: boolean
                    port:
                      type: string
                    scheme:
                      type: string
                replacePath:
                  type: object
                  description: ReplacePath holds the ReplacePath configuration.
                  properties:
                    path:
                      type: string
                replacePathRegex:
                  type: object
                  description: ReplacePathRegex holds the ReplacePathRegex configuration.
                  properties:
                    regex:
                      type: string
                    replacement:
                      type: string
                retry:
                  type: object
                  description: Retry holds the retry configuration.
                  properties:
                    attempts:
                      type: integer
                    initialInterval:
                      anyOf:
                        - type: integer
                        - type: string
                      x-kubernetes-int-or-string: true
                stripPrefix:
                  type: object
                  description: StripPrefix holds the StripPrefix configuration.
                  properties:
                    forceSlash:
                      type: boolean
                    prefixes:
                      type: array
                      items:
                        type: string
                stripPrefixRegex:
                  type: object
                  description: StripPrefixRegex holds the StripPrefixRegex configuration.
                  properties:
                    regex:
                      type: array
                      items:
                        type: string
status:
  acceptedNames:
    kind: ""
    plural: ""
  conditions: []
  storedVersions: []
