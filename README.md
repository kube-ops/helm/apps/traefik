# traefik

[![Version: 1.4.0](https://img.shields.io/badge/Version-1.4.0-informational?style=flat-square) ](#)
[![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ](#)
[![AppVersion: 2.5.1](https://img.shields.io/badge/AppVersion-2.5.1-informational?style=flat-square) ](#)
[![Artifact Hub: kube-ops](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/kube-ops&style=flat-square)](https://artifacthub.io/packages/helm/kube-ops/traefik)

A Traefik based Kubernetes ingress controller

## Installing the Chart

To install the chart with the release name `my-release`:

```console
$ helm repo add kube-ops https://charts.kube-ops.io
$ helm repo update
$ helm upgrade my-release kube-ops/traefik --install --namespace my-namespace --create-namespace --wait
```

## Uninstalling the Chart

To uninstall the chart:

```console
$ helm uninstall my-release --namespace my-namespace
```

## Updating CRDs

```console
$ kubectl apply -k https://gitlab.com/kube-ops/helm/apps/traefik
```

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| accessLog.bufferingSize | int | `100` | Log buffering. Only used if filePath is set. |
| accessLog.fields.defaultMode | string | `"drop"` |  |
| accessLog.fields.headers.defaultMode | string | `"drop"` |  |
| accessLog.fields.headers.names | object | `{}` |  |
| accessLog.fields.names | object | `{}` |  |
| accessLog.format | string | `"json"` | Log format (common/json) Common log format: <remote_IP_address> - <client_user_name_if_available> [<timestamp>] "<request_method> <request_path> <request_protocol>" <origin_server_HTTP_status> <origin_server_content_size> "<request_referrer>" "<request_user_agent>" <number_of_requests_received_since_Traefik_started> "<Traefik_router_name>" "<Traefik_server_URL>" <request_duration_in_ms>ms |
| affinity | object | `{}` |  |
| api.dashboard | bool | `false` |  |
| api.debug | bool | `false` |  |
| api.insecure | bool | `false` |  |
| autoscaling.enabled | bool | `false` | Specifies the horizontal pod autoscaling is enabled. Only used if deploymentType == Deployment. |
| autoscaling.maxReplicas | int | `100` |  |
| autoscaling.minReplicas | int | `1` |  |
| autoscaling.targetCPUUtilizationPercentage | int | `80` |  |
| checkNewVersion | bool | `false` |  |
| deploymentType | string | `"Deployment"` | Type of the application controller. Possible values: Deployment, DaemonSet |
| dnsConfig | object | `{}` |  |
| dnsPolicy | string | `""` |  |
| dynamicConfig | string | `""` | Dynamic configuration |
| entryPoints.http.address | string | `":8080"` |  |
| entryPoints.http.hostPort | int | `8080` |  |
| entryPoints.http.nodePort | int | `30080` |  |
| entryPoints.http.servicePort | int | `80` |  |
| entryPoints.https.address | string | `":8443"` |  |
| entryPoints.https.hostPort | int | `8443` |  |
| entryPoints.https.nodePort | int | `30443` |  |
| entryPoints.https.servicePort | int | `443` |  |
| entryPoints.metrics.address | string | `":9042"` |  |
| entryPoints.ping.address | string | `":8001"` |  |
| entryPoints.traefik.address | string | `":8000"` |  |
| extraArgs | list | `[]` |  |
| extraEnvVars | list | `[]` |  |
| extraPorts | list | `[]` |  |
| extraVolumeMounts | list | `[]` |  |
| extraVolumes | list | `[]` |  |
| fullnameOverride | string | `""` | Overrides the full name |
| global.imagePullPolicy | string | `"IfNotPresent"` | Image download policy ref: https://kubernetes.io/docs/concepts/containers/images/#updating-images |
| global.imagePullSecrets | list | `[]` | List of the Docker registry credentials ref: https://kubernetes.io/docs/concepts/containers/images/#updating-images |
| hostAliases | list | `[]` |  |
| hostNetwork | bool | `false` |  |
| hostResolver.cnameFlattening | bool | `false` | Enable CNAME Flattening. |
| hostResolver.resolvConfig | string | `"/etc/resolv.conf"` | resolv.conf used for DNS resolving |
| hostResolver.resolvDepth | int | `5` | The maximal depth of DNS recursive resolving |
| image.repository | string | `"quay.io/kube-ops/traefik"` | Overrides the image repository |
| image.tag | string | `""` | Overrides the image tag whose default is the chart appVersion. |
| ingress.annotations | object | `{}` |  |
| ingress.basicAuth.enabled | bool | `true` | Creates basic-auth middleware for securing Traefik dashboard and API |
| ingress.basicAuth.secretName | string | `""` | Secret name that contains basic-auth users |
| ingress.basicAuth.users | list | `[]` | Creates basic-auth secret with given users. |
| ingress.enabled | bool | `false` | Specifies whether a ingress should be created |
| ingress.labels | object | `{}` |  |
| ingress.tls | object | `{}` |  |
| ingressClass.enabled | bool | `false` |  |
| ingressClass.name | string | `""` |  |
| ingressClass.setAsDefault | bool | `true` |  |
| initContainers | list | `[]` | Specifies init containers |
| installCRDs | bool | `true` |  |
| livenessProbe | object | `{}` |  |
| log.format | string | `"json"` | Log format (common/json) |
| log.level | string | `"WARN"` | Minimal level of the events to write to log. Possible values: DEBUG, PANIC, FATAL, ERROR, WARN, and INFO |
| metrics.datadog | object | `{}` |  |
| metrics.influxDB | object | `{}` |  |
| metrics.prometheus.addEntryPointsLabels | bool | `true` |  |
| metrics.prometheus.addRoutersLabels | bool | `true` |  |
| metrics.prometheus.addServicesLabels | bool | `true` |  |
| metrics.prometheus.buckets[0] | float | `0.1` |  |
| metrics.prometheus.buckets[1] | float | `0.3` |  |
| metrics.prometheus.buckets[2] | float | `1.2` |  |
| metrics.prometheus.buckets[3] | float | `5` |  |
| metrics.prometheus.enabled | bool | `true` |  |
| metrics.prometheus.grafanaDashboard.additionalLabels.grafana_dashboard | string | `"1"` |  |
| metrics.prometheus.grafanaDashboard.enabled | bool | `false` | Install Grafana dashboard as ConfigMap |
| metrics.prometheus.grafanaDashboard.namespace | string | `"monitoring"` |  |
| metrics.prometheus.manualRouting | bool | `false` |  |
| metrics.prometheus.rules.additionalLabels | object | `{}` |  |
| metrics.prometheus.rules.alertLabels | object | `{}` |  |
| metrics.prometheus.rules.enabled | bool | `true` | Install PrometheusRule |
| metrics.prometheus.rules.namespace | string | `"monitoring"` |  |
| metrics.prometheus.rules.requestDurationMax | int | `500` | Max request duration (in milliseconds) for TraefikRequestDurationTooHigh alert firing |
| metrics.prometheus.serviceMonitor.additionalLabels | object | `{}` |  |
| metrics.prometheus.serviceMonitor.enabled | bool | `true` | Install ServiceMonitor |
| metrics.prometheus.serviceMonitor.interval | string | `"30s"` |  |
| metrics.prometheus.serviceMonitor.namespace | string | `"monitoring"` |  |
| metrics.statsD | object | `{}` |  |
| nameOverride | string | `""` | Overrides the chart name |
| nodeSelector | object | `{}` |  |
| pdb.enabled | bool | `false` | Specifies whether a pod disruption budget should be created |
| ping.entryPoint | string | `"ping"` |  |
| ping.manualRouting | bool | `false` |  |
| ping.terminatingStatusCode | int | `503` |  |
| podAnnotations | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| podSecurityPolicy.allowedCapabilities | list | `[]` | Linux capabilities, allowed by PodSecurityPolicy. If you run Traefik listened port <1024, you must add the NET_BIND_SERVICE capability. ref: https://kubernetes.io/docs/concepts/policy/pod-security-policy/#capabilities |
| podSecurityPolicy.annotations | object | `{}` |  |
| podSecurityPolicy.create | bool | `true` | Specifies whether a pod security policy should be created |
| podSecurityPolicy.enabled | bool | `true` | Specifies whether a pod security policy should be enabled |
| podSecurityPolicy.name | string | `""` | The name of the pod security policy to use. If not set and create is true, a name is generated using the fullname template |
| preStopHook | object | `{}` |  |
| priority | int | `0` |  |
| priorityClassName | string | `""` | Overrides default priority class ref: https://kubernetes.io/docs/concepts/configuration/pod-priority-preemption/ |
| providers.kubernetesCRD.allowCrossNamespace | bool | `true` |  |
| providers.providersThrottleDuration | string | `"2s"` | Duration that Traefik waits for, after a configuration reload, before taking into account any new configuration refresh event. |
| rbac.create | bool | `true` | Specifies whether a cluster role should be created |
| rbac.name | string | `""` | The name of the cluster role to use. If not set and create is true, a name is generated using the fullname template |
| rbac.namespaced | bool | `false` | Used only when rbac.create == true. If namespaced is set to true, Role will be created. ClusterRole created when namespaced is false. |
| readinessProbe | object | `{}` |  |
| replicaCount | int | `1` |  |
| resources | object | `{}` |  |
| runtimeClassName | string | `""` | Overrides default runtime class |
| schedulerName | string | `""` | Overrides default scheduler |
| securityContext | object | `{}` |  |
| sendAnonymousUsage | bool | `false` | See example of collected data: https://doc.traefik.io/traefik/contributing/data-collection/ |
| serversTransport.forwardingTimeouts | object | `{"dialTimeout":"30s","idleConnTimeout":"90s","responseHeaderTimeout":"0s"}` | is the list of certificates (as file paths, or data bytes) that will be set as Root Certificate Authorities when using a self-signed TLS certificate. rootCAs: [] - foobar.crt - foobar.crt -- A number of timeouts relevant to when forwarding requests to the backend servers. |
| serversTransport.forwardingTimeouts.dialTimeout | string | `"30s"` | Maximum duration allowed for a connection to a backend server to be established. Zero means no timeout. |
| serversTransport.forwardingTimeouts.idleConnTimeout | string | `"90s"` | maximum amount of time an idle (keep-alive) connection will remain idle before closing itself. Zero means no limit. |
| serversTransport.forwardingTimeouts.responseHeaderTimeout | string | `"0s"` | specifies the amount of time to wait for a server's response headers after fully writing the request (including its body, if any). This time does not include the time to read the response body. Zero means no timeout. |
| serversTransport.insecureSkipVerify | bool | `false` | Disables SSL certificate verification. |
| serversTransport.maxIdleConnsPerHost | int | `2` | If non-zero, maxIdleConnsPerHost controls the maximum idle (keep-alive) connections to keep per-host. |
| service.annotations | object | `{}` | Annotations for Service resource |
| service.clusterIP | string | `""` | Exposes the Service on a cluster IP ref: https://kubernetes.io/docs/concepts/services-networking/service/#choosing-your-own-ip-address |
| service.entrypoints[0] | string | `"http"` |  |
| service.entrypoints[1] | string | `"https"` |  |
| service.externalIPs | list | `[]` | If there are external IPs that route to one or more cluster nodes, Kubernetes Services can be exposed on those externalIPs ref: https://kubernetes.io/docs/concepts/services-networking/service/#external-ips |
| service.externalName | string | `""` | Services of type ExternalName map a Service to a DNS name ref: https://kubernetes.io/docs/concepts/services-networking/service/#externalname |
| service.externalTrafficPolicy | string | `"Cluster"` | If you set service.spec.externalTrafficPolicy to the value Local, kube-proxy only proxies proxy requests to local endpoints, and does not forward traffic to other nodes. This approach preserves the original source IP address. If there are no local endpoints, packets sent to the node are dropped, so you can rely on the correct source-ip in any packet processing rules you might apply a packet that make it through to the endpoint. ref: https://kubernetes.io/docs/tutorials/services/source-ip/#source-ip-for-services-with-type-nodeport |
| service.ipFamily | string | `""` | Address family for the Service's cluster IP (IPv4 or IPv6) ref: https://kubernetes.io/docs/concepts/services-networking/dual-stack/ |
| service.loadBalancerSourceRanges | list | `[]` | If is not set, Kubernetes allows traffic from 0.0.0.0/0 to the Node Security Group(s). ref: https://kubernetes.io/docs/concepts/services-networking/service/#loadbalancer |
| service.topology | bool | `false` | enables a service to route traffic based upon the Node topology of the cluster ref: https://kubernetes.io/docs/concepts/services-networking/service-topology/#using-service-topology Kubernetes >= kubeVersion 1.18 |
| service.topologyKeys | list | `[]` |  |
| service.type | string | `"ClusterIP"` | Kubernetes ServiceTypes allow you to specify what kind of Service you want ref: https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.create | bool | `true` | Specifies whether a service account should be created |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| terminationGracePeriodSeconds | int | `30` |  |
| tmpVolume | object | `{}` |  |
| tolerations | list | `[]` |  |
| updateStrategy | object | `{}` |  |

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.kube-ops.io | generate | ~0.2.3 |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.5.0](https://github.com/norwoodj/helm-docs/releases/v1.5.0)
