FROM alpine:3.14 AS builder

RUN apk --no-cache add ca-certificates tzdata

ARG VERSION=2.5.0

ADD https://github.com/traefik/traefik/releases/download/v${VERSION}/traefik_v${VERSION}_linux_amd64.tar.gz /traefik_v${VERSION}_linux_amd64.tar.gz
ADD https://github.com/traefik/traefik/releases/download/v${VERSION}/traefik_v${VERSION}_checksums.txt /checksums.txt

RUN set -eux \
  ; mkdir -p /etc/traefik \
  ; grep linux_amd64.tar.gz /checksums.txt | sha256sum -c - \
  ; tar xzvf /traefik_v${VERSION}_linux_amd64.tar.gz -C /usr/local/bin traefik \
  ; chmod +x /usr/local/bin/traefik

COPY examples/traefik.yaml /etc/traefik/traefik.yaml

FROM scratch AS artifacts

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /usr/local/bin/traefik /traefik
COPY --from=builder /etc/traefik/traefik.yaml /etc/traefik/traefik.yaml

FROM scratch

COPY --from=artifacts / /

USER 65532:65532
EXPOSE 8000
VOLUME ["/tmp"]
ENTRYPOINT ["/traefik"]
