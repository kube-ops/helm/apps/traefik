{{- define "traefik.dynamicConfig" -}}
{{- .Values.dynamicConfig -}}
{{- end -}}

{{- define "traefik.configGlobal" -}}
global:
  checkNewVersion: {{ .Values.checkNewVersion | default false | toString }}
  sendAnonymousUsage: {{ .Values.sendAnonymousUsage | default true | toString }}
{{- end -}}

{{- define "traefik.configEntryPoints" -}}
entryPoints:
  {{ range $entrypoint, $config := .Values.entryPoints }}
  {{ $entrypoint }}:
    address: {{ $config.address }}

    {{- if $config.transport }}
    transport:
      {{- toYaml $config.transport | nindent 6 }}
    {{- end }}

    {{- if $config.proxyProtocol }}
    proxyProtocol:
      {{- toYaml $config.proxyProtocol | nindent 6 }}
    {{- end }}

    {{- if $config.forwardedHeaders }}
    forwardedHeaders:
      {{- toYaml $config.forwardedHeaders | nindent 6 }}
    {{- end }}

    {{- if $config.http }}
    http:
      {{- toYaml $config.http | nindent 6 }}
    {{- end }}
  {{ end }}
{{- end -}}

{{- define "traefik.configMetrics" -}}
{{- with .Values.metrics -}}
metrics:
  {{- with .prometheus }}
  {{-   if .enabled }}
  prometheus:
    {{- with .buckets }}
    buckets:
      {{- toYaml . | nindent 10 }}
    {{- end }}
    {{- if eq .addEntryPointsLabels false }}
    addEntryPointsLabels: false
    {{- end }}
    {{- if eq .addServicesLabels false }}
    addServicesLabels: false
    {{- end }}
    {{- if eq .manualRouting true }}
    manualRouting: true
    {{- end }}
    entryPoint: metrics
  {{-   end }}
  {{- end }}
  {{- with .datadog }}
  datadog:
    {{- toYaml . | nindent 8 }}
  {{- end }}
  {{- with .statsD }}
  statsD:
    {{- toYaml . | nindent 8 }}
  {{- end }}
  {{- with .influxDB }}
  influxDB:
    {{- toYaml . | nindent 8 }}
  {{- end }}
{{- end }}
{{- end -}}

{{- define "traefik.staticConfig" -}}
{{- $global := include "traefik.configGlobal" . | fromYaml -}}
{{- $entryPoints := include "traefik.configEntryPoints" . | fromYaml -}}
{{- $metrics := include "traefik.configMetrics" . | fromYaml -}}
{{- $serversTransport := dict "serversTransport" .Values.serversTransport -}}
{{- $providers := dict "providers" .Values.providers -}}
{{- $log := dict "log" .Values.log -}}
{{- $accessLog := dict "accessLog" .Values.accessLog -}}
{{- $ping := dict "ping" .Values.ping -}}
{{- $api := dict "api" .Values.api -}}
{{- $tracing := dict "tracing" .Values.tracing -}}
{{- $hostResolver := dict "hostResolver" .Values.hostResolver -}}
{{- $certificatesResolvers := dict "certificatesResolvers" .Values.certificatesResolvers -}}
{{- $pilot := dict "pilot" .Values.pilot -}}
{{- $experimental := dict "experimental" .Values.experimental -}}
{{/* Render */}}
{{ $global | toYaml }}
{{- if $serversTransport.serversTransport }}
{{ $serversTransport | toYaml }}
{{- end }}
{{ $providers | toYaml }}
{{- if $log.log }}
{{ $log | toYaml }}
{{- end }}
{{- if $accessLog.accessLog }}
{{ $accessLog | toYaml }}
{{- end }}
{{ $entryPoints | toYaml }}
{{- if $ping.ping }}
{{ $ping | toYaml }}
{{- end }}
{{ $metrics | toYaml }}
{{- if $api.api }}
{{ $api | toYaml }}
{{- end }}
{{- if $tracing.tracing }}
{{ $tracing | toYaml }}
{{- end }}
{{- if $hostResolver.hostResolver }}
{{ $hostResolver | toYaml }}
{{- end }}
{{- if $certificatesResolvers.certificatesResolvers }}
{{ $certificatesResolvers | toYaml }}
{{- end }}
{{- if $pilot.pilot }}
{{ $pilot | toYaml }}
{{- end }}
{{- if $experimental.experimental }}
{{ $experimental | toYaml }}
{{- end }}
{{- end -}}
