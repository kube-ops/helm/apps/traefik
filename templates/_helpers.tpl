{{/* vim: set filetype=mustache: */}}
{{- define "traefik.args" -}}
{{- $args := dict "args" (.Values.extraArgs | default list) -}}
{{- toYaml $args -}}
{{- end -}}

{{/*
Create the name of the ingressClass
*/}}
{{- define "traefik.ingressClassName" -}}
{{ default (include "generate.fullname" .) .Values.ingressClass.name }}
{{- end -}}

{{/*
Create the name of the basic auth middleware
*/}}
{{- define "traefik.authMiddlewareName" -}}
{{- include "generate.fullname" . | printf "%s-basic-auth" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create the name of the basic auth secret
*/}}
{{- define "traefik.authMiddlewareSecretName" -}}
{{- $default := include "traefik.authMiddlewareName" . -}}
{{- default $default .Values.ingress.basicAuth.secretName -}}
{{- end -}}

{{/*
Create the basic auth users
*/}}
{{- define "traefik.basicAuthUsers" -}}
{{- range $i, $user := .Values.ingress.basicAuth.users }}
{{- htpasswd $user.name $user.password }}
{{- end }}
{{- end -}}
