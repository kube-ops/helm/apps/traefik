apiVersion: apps/v1
kind: {{ .Values.deploymentType }}
metadata:
  name: {{ include "generate.fullname" . }}
  labels:
    {{- include "generate.labels" . | nindent 4 }}
spec:
  {{- if and (not .Values.autoscaling.enabled) (eq .Values.deploymentType "Deployment") }}
  replicas: {{ default 1 .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "generate.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      creationTimestamp: null
      labels:
        {{- include "generate.selectorLabels" . | nindent 8 }}
      annotations:
        config/checksum: {{ include (print $.Template.BasePath "/configmap.yaml") . | sha256sum }}
        {{- with .Values.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
    spec:
      serviceAccountName: {{ include "generate.serviceAccountName" . }}
      terminationGracePeriodSeconds: {{ .Values.terminationGracePeriodSeconds }}
      hostNetwork: {{ .Values.hostNetwork }}
      volumes:
        - name: config
          configMap:
            name: {{ include "generate.fullname" . }}
        - name: tmp
          {{- if .Values.tmpVolume }}
          {{- toYaml .Values.tmpVolume | nindent 10 }}
          {{- else }}
          emptyDir: {}
          {{- end }}
        {{- with .Values.extraVolumes }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
      {{- with .Values.initContainers }}
      initContainers:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      containers:
        - name: traefik
          image: {{ include "generate.image" . | quote }}
          imagePullPolicy: {{ .Values.global.imagePullPolicy }}
          {{- include "traefik.args" . | nindent 10 }}
          {{- with .Values.extraEnvVars }}
          env:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          {{- with .Values.resources }}
          resources:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          ports:
          {{- $hostNetwork := .Values.hostNetwork }}
          {{- range $entrypoint, $config := .Values.entryPoints }}
              {{- $address := split ":" ($config.address | trimSuffix "/udp") }}
              {{- $port := $address._1 }}
            - name: {{ $entrypoint }}
              containerPort: {{ $port }}
              {{- if and $hostNetwork $config.hostPort }}
              hostPort: {{ $config.hostPort }}
              {{- end }}
              {{- if contains "udp" $config.address }}
              protocol: UDP
              {{- end }}
          {{- end }}
          {{- with .Values.extraPorts }}
            {{- toYaml . }}
          {{- end }}
          {{- if .Values.livenessProbe }}
          livenessProbe:
            {{- toYaml .Values.livenessProbe | nindent 12 }}
          {{- else if .Values.entryPoints.ping }}
          livenessProbe:
            tcpSocket:
              port: ping
            initialDelaySeconds: 10
            periodSeconds: 10
            timeoutSeconds: 2
          {{- end }}
          {{- if .Values.readinessProbe }}
          readinessProbe:
            {{- toYaml .Values.readinessProbe | nindent 12 }}
          {{- else if .Values.entryPoints.ping }}
          readinessProbe:
            failureThreshold: 1
            httpGet:
              path: /ping
              port: ping
              scheme: HTTP
            initialDelaySeconds: 10
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 2
          {{- end }}
          {{- if .Values.preStopHook }}
          lifecycle:
            preStop:
              {{- toYaml .Values.preStopHook | nindent 14 }}
          {{- end }}
          {{- with .Values.resources }}
          resources:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          {{- with .Values.securityContext }}
          securityContext:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          volumeMounts:
            - name: config
              mountPath: /etc/traefik
              readOnly: true
            - name: tmp
              mountPath: /tmp
            {{- if .Values.extraVolumeMounts }}
            {{- toYaml .Values.extraVolumeMounts | nindent 12 }}
            {{- end }}
      {{- with .Values.global.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.podSecurityContext }}
      securityContext:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- if eq .Values.hostNetwork true }}
      hostNetwork: true
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- if ne (int .Values.priority) 0 }}
      priority: {{ .Values.priority }}
      {{- end }}
      {{- if .Values.priorityClassName }}
      priorityClassName: {{ .Values.priorityClassName }}
      {{- end }}
      {{- if .Values.runtimeClassName }}
      runtimeClassName: {{ .Values.runtimeClassName }}
      {{- end }}
      {{- if .Values.schedulerName }}
      schedulerName: {{ .Values.schedulerName }}
      {{- end }}
      terminationGracePeriodSeconds: {{ .Values.terminationGracePeriodSeconds }}
      {{- if .Values.dnsPolicy }}
      dnsPolicy: {{ .Values.dnsPolicy }}
      {{- end }}
      {{- with .Values.dnsConfig }}
      dnsConfig:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.hostAliases }}
      hostAliases:
        {{- toYaml . | nindent 8 }}
      {{- end }}
  {{- if eq .Values.deploymentType "Deployment" }}
  {{- with .Values.updateStrategy }}
  strategy:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  {{- else }}
  {{- with .Values.updateStrategy }}
  updateStrategy:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  {{- end }}
